import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route } from 'react-router-dom';
import Landing from './Landing';
import About from './About';

const App = () => (
  <Router>
    <div>

        <ul>
          <li><a href="#/">Home</a></li>
          <li><a href="#/about">About</a></li>
        </ul>

        <hr/>

      <Route exact path="/" component={Landing}/>
      <Route path="/about" component={About}/>
    </div>
  </Router>
)


ReactDOM.render(<App />, document.getElementById('root'));


// The ES5 way
// var App = React.createClass({
//     render: function(){
//         return(
//             <div>
//               This is a react app, have fun :D
//             </div>
//        )
//     }
// });


//https://reacttraining.com/react-router/examples
