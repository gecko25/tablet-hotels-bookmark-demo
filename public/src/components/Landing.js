import React from 'react';

class Landing extends React.Component {

    render() {
        const message = "This is a react app. Have a ball!"
        return(
            <div>
                {message}
                <i className="material-icons">face</i>
            </div>
        )
    }
}

export default Landing;
