var express = require('express');
var config = require('./config');
var bodyParser = require('body-parser');
var cookieSession = require('cookie-session');

module.exports = function (app){
  var sess = {
    key: 'session',
    secret: 'my secret words here',
    cookie: {}
  }
//TODO: need to see the cookie.secure value to true (https only) for production -- https://github.com/expressjs/session

  app.use(express.static( config.rootPath + '/public/dist'))
     .use(cookieSession(sess))

  app.use(bodyParser.urlencoded({extended:true}));
  app.use(bodyParser.json());

     //TODO: body-parser configuration, template engine configuration
}
