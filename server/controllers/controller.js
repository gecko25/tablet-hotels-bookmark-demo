var request = require('request');
var headers = {
    'Content-Type': 'application/json'
};

exports.getSomething = function getSomething(req,res){
    var options = {
        url: "",
        headers: headers,
        json: true
    };

    request.get(options, function(err, response, body) {
        if (err)
            res.status(500).send(err);
        if (body){
            res.status(200).json(body)
        }
    });
};

exports.putSomething = function putSomething(req,res){
    var options = {
        url: "",
        headers: headers,
        json: true
    };

    request.put(options, function(err, response, body) {
        if (err)
            res.status(500).send(err);
        if (body){
            res.status(200).json(body)
        }
    });
};

exports.postSomething = function postSomething(req,res){
    var options = {
        url: "",
        headers: headers,
        json: true
    };

    request.put(options, function(err, response, body) {
        if (err)
            res.status(500).send(err);
        if (body){
            res.status(200).json(body)
        }
    });
};
