//add GETS, POSTS, PUTS, etc here
var controller = require('../controllers/controller');

module.exports = function(app) {

    app.get('/api/something',  controller.getSomething);

    app.put('/api/something',  controller.putSomething);

    app.post('/api/something',  controller.postSomething);

}
